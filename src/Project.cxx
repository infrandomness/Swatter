//
// Created by infrandomness on 07/07/22.
//

#include "../include/Project.hxx"

#include <algorithm>
#include <filesystem>
#include <list>
#include <string_view>
#include <unordered_set>
#include <utility>
#include <vector>

const std::list<std::string> Project::packageManifests = {
    "Package.swift", "Podfile", "Cartfile"};

Project::Project(std::filesystem::path projectPath)
    : projectPath{std::move(projectPath)} {}

bool Project::isValidProject() {
    return std::ranges::any_of(packageManifests,
                               [this](std::string_view packageManifest) {
        return (
            std::filesystem::is_regular_file(projectPath / packageManifest));
    });
}

std::vector<std::filesystem::path> Project::collectSourceFiles(
    const std::unordered_set<std::filesystem::path> &exclusions) const {
    std::vector<std::filesystem::path> temp;

    for (auto const &entry :
         std::filesystem::recursive_directory_iterator(projectPath)) {
        if (entry.path().extension() == ".swift" &&
            !exclusions.contains(entry)) {
            temp.push_back(entry);
        }
    }
    return temp;
}