//
// Created by infrandomness on 07/07/22.
//

#ifndef SWATTER_PROJECT_HXX
#define SWATTER_PROJECT_HXX

#include <filesystem>
#include <list>
#include <string>
#include <unordered_set>
#include <vector>

class Project final {
private:
    const static std::list<std::string> packageManifests;

public:
    std::filesystem::path projectPath;

    explicit Project(std::filesystem::path projectPath);
    Project() : Project("."){};

    /// Verifies whether or not a directory contains a valid Swift project by
    /// checking if a package manifest file exists
    /// \return True if the project contains a package manifest, false otherwise
    bool isValidProject();

    /// Collects the Swift source files in the current `projectPath` directory
    /// by iterating over all of the files and directories, ignoring the
    /// excluded files
    /// \param exclusions Vector containing all of the files to ignore
    /// \return A vector containing the collected set of files
    [[nodiscard]] std::vector<std::filesystem::path> collectSourceFiles(
        const std::unordered_set<std::filesystem::path> &exclusions) const;
};

#endif // SWATTER_PROJECT_HXX
