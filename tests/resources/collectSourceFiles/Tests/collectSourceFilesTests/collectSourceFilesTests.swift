import XCTest
@testable import collectSoureFiles

final class collectSourceFilesTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(collectSoureFiles().text, "Hello, World!")
    }
}
