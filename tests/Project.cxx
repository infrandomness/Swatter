//
// Created by infrandomness on 08/07/22.
//

#include "../include/Project.hxx"

#include <catch2/catch.hpp>
#include <filesystem>
#include <unordered_set>

TEST_CASE("isValidProject tests") {
    Project SwiftPM = Project("tests/resources/isValidProject/SwiftPM/");
    Project PodFile = Project("tests/resources/isValidProject/Podfile/");
    Project CartFile = Project("tests/resources/isValidProject/Cartfile/");
    Project InvalidProject = Project("tests/resources/isValidProject/");

    REQUIRE(SwiftPM.isValidProject() == true);
    REQUIRE(PodFile.isValidProject() == true);
    REQUIRE(CartFile.isValidProject() == true);
    REQUIRE(InvalidProject.isValidProject() == false);
}

TEST_CASE("collectSourceFiles: Collection test") {
    Project TestProject = Project("tests/resources/collectSourceFiles/");

    std::vector<std::filesystem::path> SwiftFiles = {
        std::filesystem::path(
            "tests/resources/collectSourceFiles/Package.swift"),
        std::filesystem::path("tests/resources/collectSourceFiles/Sources/"
                              "collectSourceFiles/collectSourceFiles.swift"),
        std::filesystem::path(
            "tests/resources/collectSourceFiles/Tests/CollectSourceFilesTests/"
            "CollectSourceFilesTests.swift")};

    auto Sources = TestProject.collectSourceFiles(
        std::unordered_set<std::filesystem::path>());

    REQUIRE(Sources == SwiftFiles);
}

TEST_CASE("collectSourceFiles: exclusions test") {
    Project TestProject = Project("tests/resources/collectSourceFiles/");

    std::vector<std::filesystem::path> SwiftFilesExclusions = {
        std::filesystem::path("tests/resources/collectSourceFiles/Sources/"
                              "collectSourceFiles/collectSourceFiles.swift"),
        std::filesystem::path(
            "tests/resources/collectSourceFiles/Tests/CollectSourceFilesTests/"
            "CollectSourceFilesTests.swift")};

    auto SourcesExclusions = TestProject.collectSourceFiles(
        std::unordered_set<std::filesystem::path>{
            "tests/resources/collectSourceFiles/Package.swift"});

    REQUIRE(SourcesExclusions == SwiftFilesExclusions);
}