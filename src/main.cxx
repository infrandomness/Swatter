#include "Project.hxx"

#include <iostream>
#include <unordered_set>

int main() {
    // TODO: Parse arguments from cli
    Project project = Project(".");

    if (!project.isValidProject()) {
        std::cout << "Could not find a project at " << project.projectPath
                  << "\n";
        std::exit(EXIT_FAILURE);
    }

    auto source =
        project.collectSourceFiles(std::unordered_set<std::filesystem::path>());

    for (const auto &item : source)
        std::cout << item << '\n';

    return 0;
}
